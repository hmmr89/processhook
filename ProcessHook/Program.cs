﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Globalization;


namespace ProcessHook
{
    class Program
    {
        const int PROCESS_WM_READ = 0x0010;
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess,
        int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead); 

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress,
          byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);

        static int Main(string[] args)
        {
            Program program = new Program();
            if (args.Length == 1) {
                if (args[0] == "-getproc"){
                    program.getProcessNames();
                }
            }
            else if (args.Length == 3) {
                if (args[0] == "-getInt") {
                    String progName = args[1];
                    int progAdress = int.Parse(args[2], NumberStyles.HexNumber);
                    return program.getInt(progName, progAdress);
                }
            }
            else if (args.Length == 4) {
                if (args[0] == "-setInt"){
                    String progName = args[1];
                    int progAdress = int.Parse(args[2], NumberStyles.HexNumber);
                    int value = int.Parse(args[3]);
                    return program.setInt(progName, progAdress, value);
                }
            }
            return int.MinValue;
            //p.setInt("program-name", 0x0174D64C,10);
        }


        public void getProcessNames() {
            System.IO.StreamWriter file = new System.IO.StreamWriter("processnames.txt");
            foreach(Process p in Process.GetProcesses()){
                file.WriteLine(p.ProcessName);
            }
            file.Close();
        }


        public int setInt(string processName, int address, int value)
        {
            try
            {
                Process process = Process.GetProcessesByName(processName)[0];
                IntPtr processHandle = OpenProcess(0x1F0FFF, false, process.Id);

                int bytesWritten = 0;
                byte[] buffer = BitConverter.GetBytes(value);

                WriteProcessMemory((int)processHandle, address, buffer, buffer.Length, ref bytesWritten);
                return 1;
            }
            catch (Exception ex) {
                return int.MinValue + 1;
            }
        }

        public int getInt(string processName, int address)
        {
            try
            {
                Process process = Process.GetProcessesByName(processName)[0];
                IntPtr processHandle = OpenProcess(PROCESS_WM_READ, false, process.Id);

                int bytesRead = 0;
                byte[] buffer = new byte[4];

                ReadProcessMemory((int)processHandle, address, buffer, buffer.Length, ref bytesRead);

                return BitConverter.ToInt32(buffer, 0);
            }
            catch (Exception ex) {
                return int.MinValue+1;
            }
        }

        public String getString(string processName, int address, int size)
        {
            Process process = Process.GetProcessesByName(processName)[0];
            IntPtr processHandle = OpenProcess(PROCESS_WM_READ, false, process.Id);

            int bytesRead = size;
            byte[] buffer = new byte[24];

            ReadProcessMemory((int)processHandle, address, buffer, buffer.Length, ref bytesRead);
            
            return Encoding.Unicode.GetString(buffer);
        }
    }
}
